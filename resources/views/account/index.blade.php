@extends('layouts.default')
@section('content')
    <div class="container">
        <img class="mb-4 logo-login" src="/images/logo.jpg" alt="" width="72" height="72">

        @if (session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        @endif

        @if(isset($gift))
            <div class="jumbotron">
                <h1 class="display-4">
                    {{ $message }} <br>
                    {{ $gift->name }}, в кол-ве {{ $gift->amount }}
                </h1>
                <hr class="my-4">
                @if (0)
                    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                @endif
                <a class="btn btn-primary btn-lg" href="#" role="button">Взять приз</a>
                <a class="btn btn-danger btn-lg" href="#" role="button">Отказаться</a>
            </div>
        @else
            <h1 class="h3 mb-3 font-weight-normal">Добро пожаловать, {{ Auth::user()->email }}</h1>
            <a class="btn btn-lg btn-primary btn-block" href="{!! route('game.gift-random.get') !!}">Получить случайный приз</a>
        @endif

        <br>
        <a href="{{ route('logout') }}">Выход</a>
        <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
    </div>
@stop