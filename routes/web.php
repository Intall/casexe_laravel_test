<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Route::middleware(['guest'])->group(function () {
    Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Auth\RegisterController@register');
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/my/account', 'AccountController@index')->name('account');
    Route::get('/logout', function (){
        Auth::logout();
        return redirect(route('login'));
    })->name('logout');

    Route::get('/game-gift-random/get', 'Games\GiftController@getGift')->name('game.gift-random.get');
    Route::get('/game-gift-random/take', 'Games\GiftController@takeGift')->name('game.gift-random.take');
    Route::get('/game-gift-random/cancel', 'Games\GiftController@cancelGift')->name('game.gift-random.cancel');
    Route::get('/game-gift-random/exchange-money-to-bonus', 'Games\GiftController@exchangeMoneyToBonus')->name('game.gift-random.exchange-money-to-bonus');
});