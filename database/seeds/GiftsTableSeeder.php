<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gifts')->insert([
            'name' => 'Рубли',
            'type' => 'money',
            'amount' => 999,
            'min' => 1,
            'max' => 10,
        ]);

        DB::table('gifts')->insert([
            'name' => 'Бонусные баллы',
            'type' => 'bonus',
            'min' => 100,
            'max' => 1000,
        ]);

        DB::table('gifts')->insert([
            'name' => 'MacBook',
            'type' => 'things',
            'amount' => 1,
        ]);

        DB::table('gifts')->insert([
            'name' => 'iPhone',
            'type' => 'things',
            'amount' => 5,
        ]);

        DB::table('gifts')->insert([
            'name' => 'Movie ticket',
            'type' => 'things',
            'amount' => 50,
        ]);
    }
}