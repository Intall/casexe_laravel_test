<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'demo',
            'email' => 'demo@mail.ru',
            'bank_account_number' => '9999 9999 9999 9999',
            'password' => bcrypt('password'),
        ]);
    }
}
