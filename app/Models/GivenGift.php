<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GivenGift extends Model
{
    protected $table = 'given_gifts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'type',
        'name',
        'amount',
        'status',
        'user_id',
    ];
}
