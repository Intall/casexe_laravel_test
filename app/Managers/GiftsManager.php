<?php

namespace App\Managers;

use Exception;
use App\Models\Gift;
use App\Models\GivenGift;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GiftsManager
{
    public function getRandomGiftForCurrentUser()
    {
        $user = Auth::user();

        // Выбираем все подарки которые есть в наличии
        $first = Gift::whereNotNull('amount')
                ->where('amount', '>', 0);

        $gifts = Gift::whereNull('amount')
                ->union($first)
                ->get();

        foreach ($gifts as $key => &$gift){
            // Если поля min max не заполнены то значит это вещи и их мы вручаем по одной
            if (empty($gift->min) && empty($gift->max)) {
                $gift->min = 1;
                $gift->max = 1;
            }

            // Если кол-во меньше чем минимум то удаляем из выборки
            if ($gift->amount < $gift->min && $gift->amount !== NULL) {
                unset($gifts[$key]);
            }
        }

        // Если пусто значит призы закончились, но этого не может быть так как бонусы бесконечны
        if ($gifts->isEmpty()) {
            return false;
        }

        // Выбираем случайный приз
        $selectGift = $gifts->random();

        // Генерируем случайное кол-во призов из диапазона
        $amount = rand($selectGift->min, $selectGift->max);

        // Сохраняем приз в таблицу выданных призов со статусов - waiting
        $givenGift = $this->saveSelectGift($user, $selectGift,'waiting', $amount);

        // Сохраняем у пользователя приз
        $user->gift = $givenGift;

        return $givenGift;
    }

    private function saveSelectGift($user, $selectGift, $status, $amount = null)
    {
        // Сохраняем полученный приз в таблуцу выданных призов
        $givenGift = new GivenGift();
        $givenGift->type = $selectGift->type;
        $givenGift->name = $selectGift->name;
        $givenGift->status = $status;
        $givenGift->user_id = $user->getKey();
        $givenGift->gift_id = $selectGift->getKey();
        $givenGift->amount = $amount;
        $givenGift->save();

        // Вычитаем кол-во выданных призов из общего кол-ва данного приза
        if (!empty($amount) && $selectGift->amount !== NULL){
            $selectGift->amount = $selectGift->amount - $amount;
            $selectGift->save();
        }

        return $givenGift;
    }

}