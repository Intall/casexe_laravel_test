<?php

namespace App\Http\Controllers\Games;

use App\Support\Facades\Gifts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GiftController extends Controller
{
    // Случайная выборка приза
    public function getGift(){
        $user = Auth::user();

        if ($user->gift) {
            return view('account.index', ['gift' => $user->gift, 'message' => 'Вы ранее уже получили']);
        }

        $userGift =  Gifts::getRandomGiftForCurrentUser();

        if (empty($userGift)) {
            $message = 'К сожалению, все призы уже закончились.';
            return redirect(route('account'))->with('message', $message);
        }

        return view('account.index', ['gift' => $userGift, 'message' => 'Вы получили']);
    }

    // Взять приз
    public function takeGift($id){

    }

    // Отказаться от приза
    public function cancelGift($id){

    }

    // Поменять деньги на бонусы
    public function exchangeMoneyToBonus($id){

    }
}
