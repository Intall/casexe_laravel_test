<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Managers\GiftsManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('GiftsManager',function() {
            return new GiftsManager;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
